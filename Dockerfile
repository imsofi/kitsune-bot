FROM python:3.9-slim

ENV PIP_NO_CACHE_DIR=false \
    PIPENV_HIDE_EMOJIS=1 \
    PIPENV_IGNORE_VIRTUALENVS=1 \
    PIPENV_NOSPIN=1

RUN pip install -U pipenv

WORKDIR /bot

COPY Pipfile* ./
RUN pipenv install --system --deploy

COPY . .

ENTRYPOINT ["python3"]
CMD ["-m", "bot"]
