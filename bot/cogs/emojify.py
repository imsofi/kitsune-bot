from discord.ext import commands

import bot.resources.fonts as fonts

font_list = {
    'subscript': fonts.subscript,
    'emoji': fonts.emoji,
    'smallcaps': fonts.smallcaps,
    'bold': fonts.bold,
    'handwriting': fonts.handwriting,
    'italic': fonts.italic,
    'demonic': fonts.demonic,
    'fancy': fonts.fancy,
    'monospace': fonts.monospace,
}

class Emojify(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
    
    # TODO: Figure out better error handling for empty text
    @commands.command()
    async def emojify(self, ctx, font: str, *text):
        "Lets you print fancy fonts :D"
        if font in font_list:
            text = ' '.join(text).lower()
            output = text.translate(font_list[font])
        else:
            output = f'Usage:\n`emojify FONT text`.\n\nAvailable fonts:\n`{", ".join(font_list)}`'

        await ctx.send(output)

def setup(bot):
    bot.add_cog(Emojify(bot))
