from discord.ext import commands

import random

class General(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def add(self, ctx, left: int, right: int):
        """Adds two numbers together"""
        await ctx.send(left + right)


    @commands.command()
    async def roll(self, ctx, dice: str):
        """Example command; rolls a dice in NdN format."""
        try:
            rolls, limit = map(int, dice.split('d'))
        except Exception:
            await ctx.send('Format has to be NdN!')
            return

        result = ', '.join(str(random.randint(1, limit)) for _ in range(rolls))
        await ctx.send(result)


    @commands.command()
    async def sneakysay(self, ctx, *text):
        """Repeats what you said before deleting itself."""
        await ctx.message.delete()
        await ctx.send(' '.join(text))


    @commands.command()
    async def f(self, ctx):
        """Reacts to your message with an F emoji"""
        await ctx.message.add_reaction('\N{REGIONAL INDICATOR SYMBOL LETTER F}')

def setup(bot):
    bot.add_cog(General(bot))
