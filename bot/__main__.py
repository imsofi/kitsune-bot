import logging
from configparser import ConfigParser

from bot.bot import Bot

# TODO: is this the best way to handle secrets?
secrets = ConfigParser()
secrets.read('secrets.ini')
DISCORD_TOKEN = secrets['Discord']['token']

# TODO: figure out a better logging system.
logger = logging.getLogger('discord')
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename='logs/discord.log', encoding='utf-8', mode='w')
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)

bot = Bot.create()

for ext in ['bot.cogs.general', 'bot.cogs.emojify']:
    bot.load_extension(ext)

@bot.event
async def on_ready():
    print(f'Active! Using account: {bot.user.name} ({bot.user.id})')

bot.run(DISCORD_TOKEN)
