# kitsune-bot

Discord bot

## Setup

```bash
$ cp secrets.ini{,.example}
```

Edit the `secrets.ini` with your discord token

### Build
```bash
$ podman build -t kitsune_bot .
```

## Running

```bash
$ podman run --rm -d --name kitsune_bot kitsune_bot
```

## Stopping

```bash
$ podman stop kitsune_bot
```
